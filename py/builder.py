
import numpy as np
from PIL import Image, ImageDraw, ImageFont


#BG = "#1E0300"
#FG = "#FFCBBC"

BG = "#000"
FG = "#FFF"
PADRATIO = 0
ITERATIONS = 2
REVERSE = False
DO_SIGN = False
SIGNATURE = 'afshar'
FONTSIZE = 8


BASE_SHAPE = [
    [1, 1, 1, 0, 1, 1, 1],
    [1, 0, 1, 0, 1, 0, 0],
    [1, 0, 1, 0, 1, 1, 1],
    [1, 0, 1, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 0, 1, 0, 1],
]


def make_shape(base_shape):
    base_shape_array = np.fliplr(np.array(base_shape))
    base_shape_size = len(base_shape)
    base_shape_pieces = [
        base_shape_array,
    ]
    for i in range(3):
        base_shape_pieces.append(np.rot90(base_shape_array, i + 1))
    asize = 2 * base_shape_size + 1
    blank_col = np.array([[0] for i in range(base_shape_size)])
    blank_sep = np.array([[0 for i in range(asize + 2)]])
    cat = np.concatenate
    shape = cat([
        blank_sep,
        cat([
            blank_col,
            base_shape_pieces[0],
            blank_col,
            base_shape_pieces[3],
            blank_col,
        ], 1),
        blank_sep,
        cat([
            blank_col,
            base_shape_pieces[1],
            blank_col,
            base_shape_pieces[2],
            blank_col,
        ], 1),
        blank_sep,
    ], 0)
    return shape


def squash(shape, x, y, s, d, fg, bg):
    ss = len(shape[0])
    us = s / ss
    for sx in xrange(ss):
        for sy in xrange(ss):
            value = shape[sx][sy]
            ax = sx * us + x
            ay = sy * us + y
            if value == 0:
                #d.rectangle([ax, ay, ax + us - 1, ay + us - 1], bg, None)
                d.point([ax, ay], bg)
            else:
                #d.rectangle([ax, ay, ax + us - 1, ay + us - 1], fg, None)
                d.point([ax, ay], fg)
                if us >= ss:
                    squash(shape, ax, ay, us, d, fg, bg)


def sign(model, t, size):
    drawable = ImageDraw.Draw(model)
    font = ImageFont.truetype('font.ttf', FONTSIZE)
    w, h = drawable.textsize(t, font)
    drawable.text((size - w - FONTSIZE, size - h - FONTSIZE), t, FG, font)


def draw(shape, iterations, fg, bg, padratio):
    initial_size = np.power(len(shape[0]), iterations)
    if padratio:
        pad = initial_size / padratio
    else:
        pad = 0
    page_size = initial_size + 2*pad
    model = Image.new('RGB', (page_size, page_size), bg)
    drawable = ImageDraw.Draw(model)
    squash(shape, pad, pad, initial_size, drawable, fg, bg)
    if DO_SIGN:
        sign(model, SIGNATURE, page_size) 

    #print iterations
    if iterations < 3:
        model = model.resize((model.size[0]*7, model.size[1]*7))
    return model


def main():
    shape = make_shape(BASE_SHAPE)
    fg, bg = (BG, FG) if REVERSE else (FG, BG)
    model = draw(shape, ITERATIONS, fg, bg, PADRATIO) 
    #model.show()
    #model.save('out/out.jpg')
    print model.size
    model.save('out/out.png')

if __name__ == '__main__':
    main()

